﻿using System;
using System.Collections.Generic;

namespace FightClub
{
    class Program
    {
        static View view = new View();

        static List<Fighter> fighter = Fighter.fighterList();

        static List<Weapon> weapons = Weapon.weaponList();
        static DiceRoll dice = new DiceRoll();

        

        static void Main(string[] args)
        {
            //Zugreihenfolge auswählen
            int attacker;
            int defender;

            if (dice.roll(2) == 1)
            {
                attacker = 0;
                defender = 1;
            }
            else
            {
                attacker = 1;
                defender = 0;
            }

            //Begrüßung
            view.welcome();

            view.status(fighter[0].name, fighter[1].name, fighter[0].hp, fighter[1].hp, weapons[fighter[0].weapon].name, weapons[fighter[1].weapon].name);

            //Beginn Kampf
            while (fighter[0].hp > 0 && fighter[1].hp > 0)
            {
                Console.Clear();

                weaponGet(attacker);
                fight(attacker, defender);

                view.status(fighter[0].name, fighter[1].name, fighter[0].hp, fighter[1].hp, weapons[fighter[0].weapon].name, weapons[fighter[1].weapon].name);

                //Tauschen der Angriffsrollen
                if (attacker == 0)
                {
                    attacker = 1;
                    defender = 0;
                } else
                {
                    attacker = 0;
                    defender = 1;
                }
                Console.ReadLine();
                
            }

            //Kampfende

            if (fighter[0].hp > 0)
            {
                view.win(fighter[0].name);
            } else
            {
                view.win(fighter[1].name);
            }
            

            
            


        }

        //Funktion für den Kampf
        static void fight(int attacker, int defender)
        {
            //??? weapons[fighter[attacker].weapon].dmgMod u.ä. geht bestimmt einfacher
            if ((fighter[attacker].attackStat + weapons[fighter[attacker].weapon].attackMod + dice.roll(20) >= fighter[defender].defenseStat + 10 )) {
                
                int damageRoll = fighter[attacker].dmgStat + weapons[fighter[attacker].weapon].dmgMod + dice.roll(6);
                fighter[defender].hp = fighter[defender].hp - damageRoll;

                view.hit(fighter[attacker].name, fighter[defender].name, damageRoll);
                weaponLost(defender);
            }
            else
            {
                view.miss(fighter[attacker].name);
            }

        }

        //Funktion für das Finden von Waffen
        static void weaponGet(int attacker)
        {
            
            if (weapons[fighter[attacker].weapon].name == "fist")
            {
                
                if ((dice.roll(5) -1) == 0)
                {
                    fighter[attacker].weapon = dice.roll((weapons.Count) -1);
                    view.weaponFind(fighter[attacker].name, weapons[fighter[attacker].weapon].name);

                }
            }
        }

        //Funktion für das Verlieren von Waffen
        static void weaponLost(int defender)
        {
            if ((dice.roll(20) - 1) == 0 && fighter[defender].weapon != 0) {
                fighter[defender].weapon = 0;
                view.weaponLost(fighter[defender].name);

            }
        }
    }
}
