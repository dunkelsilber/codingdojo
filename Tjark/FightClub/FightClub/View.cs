﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace FightClub
{
    class View
    {
        //Willkommensbildschirm
        public void welcome()
        {
            Console.WriteLine("Welcome to the Fight Club!");
            Console.ReadLine();
        }

        //Statusanzeige Kämpfer
        public void status(string fighter1Name, string fighter2Name, int fighter1Hp, int fighter2Hp, string fighter1Weapon, string fighter2Weapon)
        {

            Console.WriteLine("   ++++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("   + " + fighter1Name + "\t\t\t" + fighter2Name + " \t\t+");
            Console.WriteLine("   + HP: " + fighter1Hp + "\t\t\t" + "HP: " + fighter2Hp + "\t\t+");
            Console.WriteLine("   + Weapon: " + fighter1Weapon + "\t\t" + "Weapon: " + fighter2Weapon + "\t+");
            Console.WriteLine("   ++++++++++++++++++++++++++++++++++++++++++++++");
        }

        //Waffen Statusveränderungen

        public void weaponLost (string name)
        {
            Console.WriteLine(name + " lost his weapon");
            
        }

        public void weaponFind(string name, string weapon)
        {
            Console.WriteLine("Awesome! " + name + " found a " + weapon);
            
        }

        //Kampf

        public void hit (string attackerName, string defenderName, int damage)
        {
            Console.WriteLine(attackerName + " hit " + defenderName + " for " + damage + " damage!");
            
        }

        public void miss (string attackerName)
        {
            Console.WriteLine(attackerName + " misses!");
            
        }

        //Gewonnen

        public void win (string winner)
        {
            Console.WriteLine(winner + " wins! ");
            Console.WriteLine("Perfect!");
            Console.ReadLine();
        }
    }
}
