﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FightClub
{
    class Weapon
    {
        public string name { get; set; }
        public int attackMod { get; set; }
        public int dmgMod { get; set; }

        public int durability { get; set; } //kann ignoriert werden. Ich dachte, hier könnte man später noch prüfen, ob eine Waffe zerbricht

        public static List<Weapon> weaponList()
        {
            List<Weapon> weaponlist = new List<Weapon>();
            
            weaponlist.Add(new Weapon() { name = "fist", attackMod = 0, dmgMod = 0, durability = 100 });
            weaponlist.Add(new Weapon() { name = "axe", attackMod = 2, dmgMod = 5, durability = 95 });
            weaponlist.Add(new Weapon() { name = "sword", attackMod = 3, dmgMod = 3, durability = 95 });
            weaponlist.Add(new Weapon() { name = "dagger", attackMod = 5, dmgMod = 0, durability = 95 });
            weaponlist.Add(new Weapon() { name = "hammer", attackMod = 0, dmgMod = 7, durability = 95 });
            
            return weaponlist;
        }
    }
}
