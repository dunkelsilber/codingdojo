﻿using System.Collections.Generic;
using System.Linq;

namespace FightClub
{
    public static class Calculator
    {

        public static int CalculateAdditiveFactorialWithAggregate(int input)
        {
            var result = 0;

            if (input > 0)
            {
                IEnumerable<int> factorialNumbers = Enumerable.Range(1, input);
                result = factorialNumbers.Aggregate((f, s) => f + s);

                
            }

            return result;
        }

        public static int CalculateAdditiveFactorialWithForLoop(int input)
        {
            var result = 0;

            for (int i = 0; i < input + 1; i++)
            {
                result += i;
            }

            return result;
        }

    }
}

