﻿using System;
using System.Dynamic;

namespace FightClub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a die roll string to be resolved (e.g. 'd6', '4d8+2' or '2d20-2'): ");
            string dieString = (Console.ReadLine());
            
            Console.WriteLine(dieString + " => " + Die.Roll(dieString));


            //Console.Write("Enter a number to be factorialized: ");

            //if (!int.TryParse(Console.ReadLine(), out var factorial)) {
            //    Console.WriteLine("Please enter a number.");
            //};
            //Console.WriteLine("Factorials: \n\n");
            //Console.WriteLine("Aggregate 5!: " + Calculator.CalculateAdditiveFactorialWithAggregate(factorial) );
            //Console.WriteLine("ForLoop 5!: " + Calculator.CalculateAdditiveFactorialWithForLoop(factorial) );
        }
    }
}
