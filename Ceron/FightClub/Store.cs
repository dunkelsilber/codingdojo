﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FightClub
{
    public static class Store
    {
        private static Random Rng = new Random();
        public static List<Weapon> Weapons = new List<Weapon>();
        public static Weapon BareFists = new Weapon("Bare Fist", 15, 0, 0);

        public static void PopulateWeaponsStore()
        {
            Weapons.Add( new Weapon("Broad Sword", 29, 2, 4));
            Weapons.Add( new Weapon("Short Sword", 26, 2, 3));
            Weapons.Add( new Weapon("Short Sword", 26, 2, 3));
            Weapons.Add( new Weapon("Scimitar", 35, 3, 2));
            Weapons.Add( new Weapon("Knife", 23, 1, 3));
            Weapons.Add( new Weapon("Knife", 23, 1, 3));
            Weapons.Add( new Weapon("Knife", 23, 1, 3));
            Weapons.Add(new Weapon("Katana", 45, 2, 5));
            Weapons.Add(new Weapon("War Hammer", 60, 1, 6));
            Weapons.Add(new Weapon("Claymore", 50, 2, 10));
            Weapons.Shuffle();
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}
