﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FightClub
{
    public class Fighter
    {
        public string Name { get; set; }
        public string PlayerName { get; set; }
        public int HitPoints { get; set; }
        public int AttackRating { get; set; }
        public int DefenseRating { get; set; }
        public bool IsStunned { get; set; }

        public Weapon Weapon { get; set; }


        /// <summary>
        /// One attack vs opponent. d20+AttackRating vs 10+DefenseRating
        /// </summary>
        /// <param name="opponent"></param>
        public void Hit(Fighter opponent)
        {
            var hitRoll = this.AttackRating + Die.Roll(20) + Weapon.AttackBonus;
            var damageRoll = Die.Roll(Weapon.DamageRating) + Weapon.DamageBonus;
            if (hitRoll >= opponent.DefenseRating + 10)
            {
                View.ShowHit(this, opponent, damageRoll);
                opponent.TryDropWeapon();               
                opponent.HitPoints -= damageRoll;
            }
            else
            {
                View.ShowMiss(this, opponent);
            }
        }

        public void TryDropWeapon()
        {
            if (HasWeapon() && Die.Roll(10) > 9)
            {
                View.ShowWeaponDrop(this);
                Weapon = Store.BareFists;
            }

        }

        public void TryPickupWeapon()
        {
                if (!HasWeapon() && Die.Roll(10) > 8)
                {
                    Weapon = Store.Weapons.FirstOrDefault();
                    Store.Weapons.RemoveAt(0);
                   View.ShowWeaponPickup(this);
                }
           
        }

        private bool HasWeapon()
        {
            return Weapon.Name != Store.BareFists.Name;
        }

        public bool IsAlive()
        {
            return HitPoints > 0;
        }

        public Fighter(string name, string playerName, int hitPoints, int attackRating, int defenseRating)
        {
            Name = name;
            PlayerName = playerName;
            HitPoints = hitPoints;
            AttackRating = attackRating;
            DefenseRating = defenseRating;
            IsStunned = false;

            Weapon = Store.BareFists;
        }
    }
}
