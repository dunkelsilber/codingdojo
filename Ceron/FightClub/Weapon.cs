﻿using System.Dynamic;

namespace FightClub
{
    public class Weapon
    {
        public Weapon(string name, int damageRating, int attackBonus, int damageBonus)
        {
            Name = name;
            DamageRating = damageRating;
            AttackBonus = attackBonus;
            DamageBonus = damageBonus;
        }

        public string Name { get; set; }
        public int DamageRating { get; set; }
        public int DamageBonus { get; set; }

        public int AttackBonus { get; set; }

    }
}