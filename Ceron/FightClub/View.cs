﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FightClub
{
    public static class View
    {

        public static void ShowHit(Fighter fighter, Fighter opponent, int damageRoll)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"{fighter.Name} hits his opponent {opponent.Name} with his {fighter.Weapon.Name}, dealing {damageRoll} damage!");
            Console.ResetColor();
        }

        public static void ShowMiss(Fighter fighter, Fighter opponent)
        {
            Console.WriteLine($"{fighter.Name} misses his opponent {opponent.Name}!");

        }

        public static void ShowWeaponPickup(Fighter fighter)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"{fighter.Name} picked up a {fighter.Weapon.Name}!");
            Console.ResetColor();
        }

        public static void ShowWelcome()
        {
            Console.WriteLine("Welcome to the Fights!\n");
            Console.WriteLine("The battle starts!");
        }

        public static void ShowVictory(Fighter leftCornerFighter, Fighter rightCornerFighter)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(!leftCornerFighter.IsAlive()
                ? $"\n{rightCornerFighter.Name} wins!"
                : $"\n{leftCornerFighter.Name} wins!");
            Console.ResetColor();
        }

        public static void ShowWeaponDrop(Fighter fighter)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"{fighter.Name} dropped his {fighter.Weapon.Name}!");
            Console.ResetColor();

        }

        public static void ViewStatus(Fighter leftCornerFighter, Fighter rightCornerFighter)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{leftCornerFighter.Name}: {leftCornerFighter.HitPoints} HP. {rightCornerFighter.Name}: {rightCornerFighter.HitPoints} HP.");
            Console.ResetColor();
        }
    }
}
