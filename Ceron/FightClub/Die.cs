﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FightClub
{
    public static class Die
    {
        private static readonly Random Rng = new Random();

        /// <summary>
        /// Single die roll without any modifiers
        /// </summary>
        /// <param name="sidesOfDie"></param>
        /// <returns></returns>
        public static int Roll(int sidesOfDie)
        {
            var result = Rng.Next(1, sidesOfDie + 1);
            return result;
        }

        /// <summary>
        /// Single set of D20-like die roll commands. Accepts input like "d6+1", "3d8+19" or "2d20-4" 
        /// </summary>
        /// <param name="rollString"> </param>
        /// <returns> int roll result</returns>
        public static int Roll(string rollString)
        {
            var result = 0;
            
            if (rollString.StartsWith("d") || rollString.StartsWith("d")) { rollString = "1" + rollString; }

            if (!rollString.Contains("d") && (!rollString.Contains("D")))
            {
                Console.WriteLine("Unexpected Input: " + rollString);
                return 0;
            }

            try
            {
                var values = rollString.Split('d', 'D', '+', '-');

                for (var i = 0; i < Convert.ToInt32(values[0]); i++)
                {
                    result += Roll(Convert.ToInt32(values[1]));
                }
                if (values.Length > 2 && rollString.Contains('+'))
                {
                    result += Convert.ToInt32(values[2]);
                }

                if (values.Length > 2 && rollString.Contains('-'))
                {
                    result -= Convert.ToInt32(values[2]);
                    
                }
                return result;
            }
            catch
            {
                Console.WriteLine("Unexpected Input: "+ rollString);
                return 0;
            }
        }
    }
}
