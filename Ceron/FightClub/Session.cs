﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FightClub
{
    public static class Session
    {
        public static Fighter LeftCornerFighter = null;
        public static Fighter RightCornerFighter = null;
        public static Fighter CurrentFighter = null;
        public static Fighter CurrentOpponent = null;

        /// <summary>
        /// Generates two Fighters. Change their values here.
        /// </summary>
        public static void PopulateCorners()
        {
            LeftCornerFighter = new Fighter("Arminius", "PlayerA", 100, 7, 10);
            RightCornerFighter = new Fighter("Brutus", "PlayerB", 100, 7, 10);
        }

        /// <summary>
        /// One instance of a fight, from start to finish. Takes turns while both fighters are alive.
        /// </summary>
        public static void StartFight()
        {
            View.ShowWelcome();
            CurrentFighter = LeftCornerFighter;
            CurrentOpponent = RightCornerFighter;
            // Chance at beginning to pick up weapons
            LeftCornerFighter.TryPickupWeapon();
            RightCornerFighter.TryPickupWeapon();
            while (LeftCornerFighter.IsAlive() && RightCornerFighter.IsAlive())
            {
                TakeTurn();
            }

            View.ShowVictory(LeftCornerFighter, RightCornerFighter);
        }

        /// <summary>
        /// One turn of trying to pickup stuff, hitting your opponent and switching roles.
        /// </summary>
        private static void TakeTurn()
        {
            Thread.Sleep(1000); //to create more tension.
            CurrentFighter.TryPickupWeapon();
            CurrentFighter.Hit(CurrentOpponent);
            View.ViewStatus(LeftCornerFighter, RightCornerFighter);

            if (CurrentFighter == LeftCornerFighter)
            {
                CurrentOpponent = LeftCornerFighter;
                CurrentFighter = RightCornerFighter;
            }

            else
            {
                CurrentOpponent = RightCornerFighter;
                CurrentFighter = LeftCornerFighter;
            }
        }
    }
}
