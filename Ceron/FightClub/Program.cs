﻿using System;

namespace FightClub
{
    class Program
    {
        static void Main(string[] args)
        {
            Session.PopulateCorners();
            Store.PopulateWeaponsStore();
            Session.StartFight();
            Console.Read();
        }
    }
}
